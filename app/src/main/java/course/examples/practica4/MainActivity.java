package course.examples.practica4;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TabHost;

public class MainActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "course.examples.practica4";
    TabHost TbH;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        TbH = (TabHost) findViewById(R.id.tabHost); //llamamos al Tabhost
        TbH.setup();                                                         //lo activamos

        TabHost.TabSpec tab1 = TbH.newTabSpec("tab1");  //aspectos de cada Tab (pestaña)
        TabHost.TabSpec tab2 = TbH.newTabSpec("tab2");
        TabHost.TabSpec tab3 = TbH.newTabSpec("tab3");

        tab1.setIndicator("Temperatura");    //qué queremos que aparezca en las pestañas
        tab1.setContent(R.id.ejemplo1); //definimos el id de cada Tab (pestaña)

        tab2.setIndicator("Area");
        tab2.setContent(R.id.ejemplo2);

        tab3.setIndicator("Distancia");
        tab3.setContent(R.id.ejemplo3);

        TbH.addTab(tab1); //añadimos los tabs ya programados
        TbH.addTab(tab2);
        TbH.addTab(tab3);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_search:
                return true;
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void enviarMensaje(View v) {
        switch (v.getId()) {
            case (R.id.button):
                Intent intent = new Intent(this, Resultado.class);
                EditText editText = (EditText) findViewById(R.id.editText);
                String message01 = editText.getText().toString();
                String message1 = "1, "+message01;
                intent.putExtra(EXTRA_MESSAGE, message1);
                startActivity(intent);

                break;
            case (R.id.button2):

                Intent intent2 = new Intent(this, Resultado.class);
                EditText editText2 = (EditText) findViewById(R.id.editText2);
                String message02 = editText2.getText().toString();
                String message2 = "2, "+message02;
                intent2.putExtra(EXTRA_MESSAGE, message2);
                startActivity(intent2);

                break;
            case (R.id.button3):

                Intent intent3 = new Intent(this, Resultado.class);
                EditText editText3 = (EditText) findViewById(R.id.editText3);
                String message03 = editText3.getText().toString();
                String message3 = "3, "+message03;
                intent3.putExtra(EXTRA_MESSAGE, message3);
                startActivity(intent3);

                break;
        }


    }
}
