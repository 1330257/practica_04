package course.examples.practica4;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class Resultado extends AppCompatActivity {

    TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Cristian Aarón Pérez y Tejeda", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        textView = (TextView) findViewById(R.id.textView4);
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        String[] mensajes = message.split(",");

        if (mensajes[0].contains("1")) {
            double uno=0;

            try{
                uno = Double.parseDouble(mensajes[1]);
                double res = (uno-32)/1.8;
                textView.setText("°"+res);

            }catch (Exception e){
                textView.setText("Usted no envio lo esperado: "+e);
            }


        }else if (mensajes[0].contains("2")){
            double uno=0;
            try{
                uno = Double.parseDouble(mensajes[1]);
                double res = uno*uno;
                textView.setText(res+" unidades cuadradas");
            }catch (Exception e){
                textView.setText("Usted no envio lo esperado: "+e);
            }

        }else{
            double uno=0;
            try{
                uno = Double.parseDouble(mensajes[1]);
                double res = uno*1.609;
                textView.setText(res+" kilometros");
            }catch (Exception e){
                textView.setText("Usted no envio lo esperado: "+e);
            }
        }

    }

}
